#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#Created on Mon Nov  8 17:39:27 2021
# Hacking Ecology Sensing API
# Copyright (c) 2021, Free Software Foundation
# All rights reserved
#
# GNU General Public License version 3
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License. You should have
# received a copy of the GPL license along with this program; if you
# did not, you can find it at https://www.gnu.org/licenses/gpl-3.0.en.html
#@author: Hacking Ecology
"""

import requests
import pandas as pd
from datetime import datetime
import pytz

def login(url, username, password):
    # Log into ThingsBoard
    return requests.post(f"{url}/api/auth/login", json={
        "username": username,
        "password": password
    }).json()['token']

def get_keys(url, token, device):
    return requests.get(f"{url}/api/plugins/telemetry/DEVICE/{device}/keys/timeseries",
                 headers={
                     'content-type': 'application/json',
                     'x-authorization': f"bearer {token}"
                 }).json()
#JSON format
def get_data_chunk(url, token, device, key, start, stop, limit):
    return requests.get(f"{url}/api/plugins/telemetry/DEVICE/{device}/values/timeseries",
             headers={
                 'content-type': 'application/json',
                 'x-authorization': f"bearer {token}"
             },
            params= {
                'keys': key,
                'startTs': start,
                'endTs': stop,
                'limit': limit,
                'agg': 'NONE'
            }).json()

#DF format
def get_data(url, token, device, key, start, stop):
    p = pd.DataFrame()

    # You have to request data backwards in time ...
    while start < stop:
        data = get_data_chunk(url, token, device, key, start, stop, 10000)

        if key not in data:
            break;

        #print(f"{key}: Loaded {len(data[key])} points")

        t = pd.DataFrame.from_records(data[key])
        t.set_index('ts', inplace=True)
        t.rename(columns={'value': key}, inplace=True)
        p = p.append(t)

        # Update "new" stop time
        stop = data[key][-1]['ts'] - 1

    return p

#JSON format to DF
def get_dataframe(url, token, device, key, start, stop, limit):
    json_get= requests.get(f"{url}/api/plugins/telemetry/DEVICE/{device}/values/timeseries",
             headers={
                 'content-type': 'application/json',
                 'x-authorization': f"bearer {token}"
             },
            params= {
                'keys': key,
                'startTs': start,
                'endTs': stop,
                'limit': limit,
                'agg': 'NONE'
            }).json()
    json_norm_df = pd.json_normalize(json_get[key])
    json_norm_df['key']= key

    
    return json_norm_df


def date_ts(date, tz):
    tzone = pytz.timezone(tz)
    datetime1 = datetime.strptime(date, "%d/%m/%Y %H:%M:%S")
    localTime = tzone.localize(datetime1)
    localTime = localTime.replace(tzinfo=None)
    TS = int(localTime.timestamp() * 1000)
    return TS























