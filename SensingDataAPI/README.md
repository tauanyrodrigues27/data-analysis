# API and Telemetry Connection

This is a script that uses an Rthingsboard library to access data from the Hacking Ecology Sensing Platform.

The script covers: data visualization, CSV file download and selected data plot.

The script is available [here](https://gitlab.com/hacking-ecology/data-analysis/-/blob/main/SensingDataAPI/NayadDataAPI.R).

The first step is to include the libraries we are going to use:

```{r}
library("Rthingsboard")
library("curl")
library("dplyr")
library("ggplot2")
```
- Once we did it, we set `stringAsFactors` FALSE and define the working directory

```{r}
options(stringsAsFactors = FALSE)
setwd("ADD/SEU/DIRETORIO") #Change with your dir here
```
- We need some credentials to access our database, then we specify them. The 'publicID' is the copy of your Client ID and 'entityID' is the copy of Device ID

```{r}
url = "https://sensing.hackingecology.com"
publicId = "ADD_YOUR_CLIENT_ID" 
entityId = "ADD_THE_DEVICE_ID"
```
- Now we define the time range for the data we want to access. Note that you have to specify the Time-Zone (TZ):

```{r}
startDate = as.POSIXct("2021-07-15 14:41:45", tz = "Europe/Madrid")
endDate = as.POSIXct("2021-07-15 14:42:40", tz = "Europe/Madrid")
```

- Then we use the`url` and `publicID` to configure the API connection:

```{r}
nayad_api = ThingsboardApi(url = url, publicId = publicId)
```

- Once we configure the API, we will set the `entityId` to connect to the device database. `keys` are the variables you are looking for.
```{r}
keys = nayad_api$getKeys(entityId = entityId)
```

- Now we have all information to get the telemetry data into a data frame. It is important to know the name of the variables we want to access. There are two options:

1. If we want to access all the data, we use `keys = keys`
```{r}
df <- nayad_api$getTelemetry(entityId,
                          keys = keys,
                          startTs = startDate,
                          endTs = endDate)
```

2. If we want a specific variable, then we use `keys = keys[grep("VARIABLE_NAME", keys)]`:
```{r}
df <- tb_api$getTelemetry(entityId,
                       keys = keys[grep("^T", keys)],
                       startTs = startDate,
                       endTs = endDate)
```

### Subseting time from telemetry

- First we specify the time, if every hour, we can use just minute `00` and the seconds. In this example we set a maximum of 10 seconds to ensures that we will not lose data if readings are delayed due to multiple processes at the same time. If you work with simple code with reandings every second, you can set `second(ts) = 00`.
```{R}
df3_subset <- filter(df, 
                     (hour(ts) == 03 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 06 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 09 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 12 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 15 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 18 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 21 & minute(ts) == 00 & second(ts) <= 10)|
                     (hour(ts) == 00 & minute(ts) == 00 & second(ts) <= 10)
                     )
```

- Now you will notice that all variables lose their classes. To fix that, follow a few simple steps:

```{r}
df3_format_time <- format(df3_subset, format="%Y-%m-%d %H:%M")
df3_set_class <- transform(df3_format_time, value = as.numeric(value),
                           key = as.character(key), ts = as.POSIXlt.character(ts))
df3_set_class$ts <- as.POSIXct(df3_set_class$ts)

class(df3_set_class$ts)
class(df3_set_class$key)
class(df3_set_class$value)
```

- Now you should see the output class for each of the variables and can move to the next step for the data to be ready for analysis and visualization.

-  Here we select which variables we want to group, and which variable we want to "summarize", in this example obtaining the average of the values for each mesocosm, every day and every three hour:

```{r}
df_subset <- df3_set_class %>%
               group_by(key,ts) %>%
               summarise(value = mean(value))

knitr::kable(head(df_subset))
```

- You can check the data to see if everything looks great, using the `ggplot2` library

```{r}
ggplot(df_subset, aes(x = ts, y = value)) +
  geom_line(aes(color = key), size = 1) +
  scale_color_brewer(palette = "Set1")
```

- Now we can export the data frame as CSV, athough it is easier to work directly inside R. For example, connecting with gitlab for collaborative work. [Here is a very basic example of how to connect Rstudio with Gitlab](https://vickysteeves.gitlab.io/repro-papers/git.html).

```{r}
write.csv(df_subset, "myData.csv")
```

