#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#Created on Mon Nov  8 17:39:27 2021
# Hacking Ecology Sensing API
# Copyright (c) 2021, Free Software Foundation
# All rights reserved
#
# GNU General Public License version 3
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License. You should have
# received a copy of the GPL license along with this program; if you
# did not, you can find it at https://www.gnu.org/licenses/gpl-3.0.en.html
#@author: Hacking Ecology
"""

from fetch import login, date_ts, get_dataframe, get_keys

# Acces to Hacking Ecology Platform
url = 'https://sensing.hackingecology.com'
username= 'YOUR_USERNAME'
password = 'YOUR_PASSWORD'

# Device Information (Device = DeviceID)
device = '784a1e0-d193-11eb-83ec-e1c4224c5d26'
key = 'temperature'

# Set time and date
start_time = '11/07/2021 9:00:00'
end_time = '11/08/2021 12:00:00'
tz = 'America/Sao_Paulo' #check all timezone using 'print(pytz.all_timezones)'

startTs = date_ts(start_time, tz)
endTs = date_ts(end_time, tz)
limit=10000000 #Limit set the max of data you will get


token = login(url, username, password);
print(token)
print(get_keys(url, token, device))

# Download Data and organize for analysis.
data_get= get_dataframe(url, token, device, key, startTs, endTs, limit)
print(data_get)

data_get.to_csv('/he_data.csv', index=False)












